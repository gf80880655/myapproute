
void main() {
  
  // // 测试1：
  // // ？？ 运算符
  // var a;
  // var b='Dart';

  // var c=a??b;

  // print(c); // 输出 Dart.


  // // 测试2：
  // a=null;
  // var d=a??b;
  // print(d);   // 输出 Dart.


  // 测试3：
  // var a=null;
  var a='aaaa';
  var b='hello !';

  // var c=a??=b;

  // A=x??10
  // 如果x不等于null，则A=x，否则A=10

  // var c=a?.b;    // 返回null


  //  时间：2021年7月27日16:41:33
  var c=a??b;   // 输出 aaaa, 如果a 不为null， 则返回a ; 如果 a为null 则返回 b。
  print(c);

 

}

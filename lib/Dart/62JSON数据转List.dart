/*
 * @Author: gf80880655 asl.kiss@163.com
 * @Date: 2023-02-24 11:01:09
 * @LastEditors: gf80880655 asl.kiss@163.com
 * @LastEditTime: 2023-02-27 13:49:56
 * @FilePath: \mytestapp\myapproute\lib\Dart\62JSON数据转List.dart
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */

import 'dart:convert';
import 'dart:ffi';

import 'package:http/http.dart';

void main() {
/*
   2023年2月24日11:04:19 假设数据格式是这样的，怎么样将JSON格式转换成 下面的样式的数组？
 */
// [
//   {
//     "userId": 1,
//     "id": 1,
//     "title": "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
//     "body": "quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto"
//   },
//   {
//     "userId": 1,
//     "id": 2,
//     "title": "qui est esse",
//     "body": "est rerum tempore vitae\nsequi sint nihil reprehenderit dolor beatae ea dolores neque\nfugiat blanditiis voluptate porro vel nihil molestiae ut reiciendis\nqui aperiam non debitis possimus qui neque nisi nulla"
//   }
//  ]

  var data = [
    {
      "userId": 1,
      "id": 1,
      "title":
          "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
      "body":
          "quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto"
    },
    {
      "userId": 1,
      "id": 2,
      "title": "qui est esse",
      "body":
          "est rerum tempore vitae\nsequi sint nihil reprehenderit dolor beatae ea dolores neque\nfugiat blanditiis voluptate porro vel nihil molestiae ut reiciendis\nqui aperiam non debitis possimus qui neque nisi nulla"
    }
  ];


  print(data is Map); // 返回 false
  print(data is Array);  // 返回 false

  print(data[1]["title"]);


  
}

class VideoInfo {
  String body;
  int id;
  String title;
  int userId;

  VideoInfo({this.body, this.id, this.title, this.userId});

  factory VideoInfo.fromJson(Map<String, dynamic> json) {
    return VideoInfo(
      body: json['body'],
      id: json['id'],
      title: json['title'],
      userId: json['userId'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['body'] = this.body;
    data['id'] = this.id;
    data['title'] = this.title;
    data['userId'] = this.userId;
    return data;
  }
}

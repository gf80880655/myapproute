import 'package:flutter/material.dart';

class IconTestPage extends StatefulWidget {
  @override
  _IconTestPageState createState() => _IconTestPageState();
}

class _IconTestPageState extends State<IconTestPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text('第三方ICON的使用')),
        body: Column(
          children: <Widget>[
            // Text('2222'),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Column(
                  children: <Widget>[
                    Icon(IconData(0xe62f, fontFamily: 'MyIcons')),
                    Icon(IconData(0xe614, fontFamily: 'MyIcons')),
                    Icon(
                      IconData(0xe654, fontFamily: 'MyIcons'),
                      size: 40.0,
                      color: Colors.black,
                    ),
                    Icon(
                      IconData(0xe67b, fontFamily: 'MyIcons'),
                      size: 40.0,
                      color: Colors.redAccent,
                    ),
                    Text(
                      '文章标题-字体大小 36',
                      style: TextStyle(fontSize: 36.0),
                    ),
                    Text(
                      '文章标题-字体大小 34',
                      style: TextStyle(fontSize: 34.0),
                    ),
                    Text(
                      '文章标题-字体大小 32',
                      style: TextStyle(fontSize: 32.0),
                    ),
                    Text(
                      '说明性文字-字体大小 24',
                      style: TextStyle(fontSize: 24.0),
                    ),
                    Text(
                      '说明性文字-字体大小 28',
                      style: TextStyle(fontSize: 28.0),
                    ),
                    Text(
                      '辅助说明-字体大小 22',
                      style: TextStyle(fontSize: 22.0),
                    ),
                    Text(
                      '辅助说明-字体大小 20',
                      style: TextStyle(fontSize: 20.0),
                    ),

                    Text(
                      '辅助说明-字体大小 16',
                      style: TextStyle(fontSize: 16.0),
                    ),

                    Text(
                      '辅助说明-字体大小 14',
                      style: TextStyle(fontSize: 14.0),
                    ),

                    Text(
                      '辅助说明-字体大小 12',
                      style: TextStyle(fontSize: 12.0),
                    ),


                  ],
                ),
              ],
            ),
          ],
        ));
  }
}
